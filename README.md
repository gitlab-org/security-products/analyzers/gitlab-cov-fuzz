# gitlab-cov-fuzz

gitlab-cov-fuzz is the official gitlab CLI that helps running [fuzz tests inside gitlab CI/CD](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/).

This repo will contain releases of the CLI (not source) and handy docker images with various fuzzing engines baked-in.

## Download

Latest `v2` release can be downloaded via the following [link](https://gitlab.com/gitlab-org/security-products/analyzers/gitlab-cov-fuzz/-/raw/v2/binaries/gitlab-cov-fuzz_Linux_x86_64) or via browsing the `binaries` directory in the current repository and the appropriate tag.

## Usage

gitlab-cov-fuzz CLI can be used either locally or from gitlab CI.
Run `gitlab-cov-fuzz --help` to get a full list of commands, or check out our docs.

## Examples

gitlab-cov-fuzz currently supports C/C++, Go, Rust and Swift

* [C/C++ example](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/c-cpp-fuzzing-example)
* [Go example](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/go-fuzzing-example)
* [Rust example](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/rust-fuzzing-example)
* [Swift example](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/swift-fuzzing-example)
